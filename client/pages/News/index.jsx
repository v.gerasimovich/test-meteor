import React from 'react'
import { useTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor'

import { News } from '/lib/news'

import PageWrapper from '/client/components/PageWrapper'
import NewsBlock from '/client/components/NewsBlock'

import './news.less'

const NewsPage = () => {
    const { news, isLoading } = useTracker(() => {
		const subscription = Meteor.subscribe('news')
		let news = News.find().fetch()

		return {
			news,
			isLoading: !subscription.ready(),
		}
    })
    
    return (
        <PageWrapper>
            {!isLoading && news.map((item, index) => (
                <NewsBlock 
                    key={`newsBlock${index}`} 
                    data={item} 
                />
            ))}
            
        </PageWrapper>
    )
}
  
export default NewsPage