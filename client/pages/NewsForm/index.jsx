import React, { useState } from 'react'
import PageWrapper from '/client/components/PageWrapper'
import TextArea from '/client/components/TextArea'
import { News } from '/lib/news'
import './newsForm.less'

const NewsForm = () => {
    const [title, setTitle] = useState('')
    const [text, setText] = useState('')
    const [smile, setSmile] = useState('')
    
    return (
        <PageWrapper>
            <form className='newsForm'>
                <TextArea 
                    label='Title'
                    onChange={(e) => {
                        setTitle(e.target.value)
                    }}
                />
                <TextArea 
                    label='Text' 
                    rows={5}
                    onChange={(e) => {
                        setText(e.target.value)
                    }}
                />
                <TextArea 
                    label='Smile' 
                    rows={1}
                    onChange={(e) => {
                        setSmile(e.target.value)
                    }}
                />
                <fieldset className='inputGroup'>
                    <button 
                        onClick={(e) => {
                            e.preventDefault()
                            if (title && text && smile) {
                                Meteor.call('addNews', smile, title, text, '370', new Date().toString())
                                // News.insert({
                                //     titleSmile: smile,
                                //     title,
                                //     text,
                                //     views: "370",
                                //     date: new Date().toString()
                                // })
                            }
                        }}
                    >
                        Send
                    </button>
                </fieldset>
                
            </form>
        </PageWrapper>
    )
}

export default NewsForm