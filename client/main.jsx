import React from 'react'
import { Meteor } from 'meteor/meteor'
import { render } from 'react-dom'
import { useRoutes } from 'hookrouter'

import NotFound from '/client/pages/NotFound'
import NewsPage from '/client/pages/News'
import NewsForm from '/client/pages/NewsForm'

const routes = {
	'/': () => <NewsPage />,
	'/add-news': () => <NewsForm />,
}

const App = () => {
    const routeResult = useRoutes(routes)
	return routeResult || <NotFound/>
}

Meteor.startup(() => {
    render(<App />, document.getElementById('react-target'));
});
