import React from 'react'

import './pageWrapper.less'

const PageWrapper = ({ children }) => {
    return (
        <main className='pageWrapper'>
            <div className='pageWrapper__content'>
                {children}
            </div>
        </main>
    )
}

export default PageWrapper