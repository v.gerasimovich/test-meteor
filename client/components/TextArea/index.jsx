import React from 'react'

import './textArea.less'

const TextArea = ({ label, rows = 2, onChange = () => {} }) => {
    return (
        <fieldset className='inputGroup'>
            <label 
                className='inputGroup__label'
                htmlFor={`textfield${label}`}
            >
                {label}
            </label>
            <textarea 
                className='inputGroup__textarea'
                id={`textfield${label}`}
                placeholder={label}
                rows={rows}
                onChange={onChange}
            />
        </fieldset>
    )
}

export default TextArea