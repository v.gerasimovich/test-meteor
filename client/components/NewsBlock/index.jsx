import React from 'react'

import './newsBlock.less'

const NewsBlock = ({ data, ...props }) => {
    return (
        <article className='newsBlock' {...props}>
            <header className='newsBlock__header'>
                <h2 className='newsBlock__title'>
                    <span 
                        className='newsBlock__title_smile'
                    >
                        {data && data.titleSmile}
                    </span>
                    {data && data.title}
                </h2>
            </header>
            <p className='newsBlock__text'>
               {data && data.text}
            </p>
            <footer className='newsBlock__footer'>
                <div className='newsBlock__views'>
                    <img 
                        className='newsBlock__views_icon' 
                        src='/images/icons/view-gray.svg' 
                        alt='views' 
                    />
                    {data && data.views}
                </div>
                <div className='newsBlock__date'>
                    {data && data.date}
                </div>
            </footer>
        </article>
    )
}

export default NewsBlock