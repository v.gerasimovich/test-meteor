import { Meteor } from 'meteor/meteor'
import { News } from '/lib/news'
import NewsJson from '/data/news.json'

Meteor.startup(() => {
    NewsJson.news.forEach((item) => {
        News.remove(item)
        News.insert(item)
    })
});

Meteor.publish('news', function(id) {
    return News.find()
})

Meteor.methods({
    addNews(titleSmile, title, text, views, date) {
        check(titleSmile, String)
        check(title, String)
        check(text, String)
        check(views, String)
        check(date, String)

        News.insert({
            titleSmile,
            title,
            text,
            views,
            date
        })
    }
})